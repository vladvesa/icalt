//
//  ScheduleItem.h
//  ICALT 2012
//
//  Created by Vlad Vesa on 6/6/12.
//  Copyright (c) 2012 UPT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "AppDelegate.h"

@interface ScheduleItem : NSObject
@property (nonatomic,assign) NSInteger sessionId;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *room;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,assign) NSInteger day;
@property (nonatomic,strong) NSString* chairName;
@property (nonatomic, strong) NSString *keynote;
@property (nonatomic,assign) BOOL isFavorite;
@property (nonatomic,strong) NSString *map;
@property (nonatomic, strong) NSString *sessionDetails;

@end
