//
//  ScheduleCell.m
//  ICALT
//
//  Created by Vesa Vlad on 6/25/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "ScheduleCell.h"

@implementation ScheduleCell
@synthesize name = _name;
@synthesize time = _time;
@synthesize room = _room;
@synthesize image = _image;
@synthesize favoriteStatus =_favoriteStatus;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
