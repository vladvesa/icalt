//
//  tweetCell.h
//  ICALT
//
//  Created by Vlad Vesa on 4/14/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TweeterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *tweetUserInfo;
@property (weak, nonatomic) IBOutlet UITextView *tweetText;
@property (weak, nonatomic) IBOutlet UIImageView *tweetUserImage;
@end
