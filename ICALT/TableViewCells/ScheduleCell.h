//
//  ScheduleCell.h
//  ICALT
//
//  Created by Vesa Vlad on 6/25/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UILabel *room;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteStatus;

@end
