//
//  tweetCell.m
//  ICALT
//
//  Created by Vlad Vesa on 4/14/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "TweeterCell.h"

@implementation TweeterCell
@synthesize tweetUserInfo = _tweetUserInfo;
@synthesize tweetText = _tweetText;
@synthesize tweetUserImage = _tweetUserImage;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
