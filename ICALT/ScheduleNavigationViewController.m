//
//  ScheduleNavigationViewController.m
//  ICALT
//
//  Created by Vlad Vesa on 4/15/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "ScheduleNavigationViewController.h"
#import "ScheduleCell.h"
#import "ScheduleDetailViewController.h"
#import "AFJSONRequestOperation.h"
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "ScheduleItem.h"
@interface ScheduleNavigationViewController ()

@end
@implementation ScheduleNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    scheduleContent = [[NSMutableArray alloc] init];
    if (self) {
        // Custom initialization
        [self loadScheduleFromDB];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [tableViewDisplay reloadData];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //[scheduleContent removeAllObjects];
    //[self reloadViewData];
    
    // Do any additional setup after loading the view from its nib.
}
#if 0
- (void)reloadViewData{
    databaseFile = [FMDatabase databaseWithPath:databasePath];
    [databaseFile open];
    FMResultSet *result = [databaseFile executeQuery:@"SELECT * FROM sessions ORDER BY sessionId"];
    while([result next]){
        ScheduleItem *item = [[ScheduleItem alloc] init];
        item.sessionId = [result intForColumn:@"sessionId"];
        item.name = [result stringForColumn:@"name"];
        item.type = [result stringForColumn:@"type"];
        item.room = [result stringForColumn:@"room"];
        item.time = [result stringForColumn:@"time"];
        item.chairName = [result stringForColumn:@"chairName"];
        item.keynote = [result stringForColumn:@"keynote"];
        item.day = [result intForColumn:@"day"];
        item.isFavorite = [result intForColumn:@"favorite"];
        [scheduleContent addObject:item];
    }
    [databaseFile close];
}
#endif
- (void)loadScheduleFromDB
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    databasePath = [documentDir stringByAppendingPathComponent:@"icalt_v2.db"];
    
        
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:databasePath];
    if(success){
        //[fileManager removeItemAtPath:databasePath error:nil]; //this is only for testing purpose and should be removed when app will be public
        //return;
    }
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"icalt.db"];
    
    [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
    
    
    databaseFile = [FMDatabase databaseWithPath:databasePath];
    [databaseFile open];
    FMResultSet *result = [databaseFile executeQuery:@"SELECT * FROM sessions ORDER BY sessionId"];
    while([result next]){
        ScheduleItem *item = [[ScheduleItem alloc] init];
        item.sessionId = [result intForColumn:@"sessionId"];
        item.name = [result stringForColumn:@"name"];
        item.type = [result stringForColumn:@"type"];
        item.room = [result stringForColumn:@"room"];
        item.time = [result stringForColumn:@"time"];
        item.chairName = [result stringForColumn:@"chairName"];
        item.keynote = [result stringForColumn:@"keynote"];
        item.day = [result intForColumn:@"day"];
        item.isFavorite = [result intForColumn:@"favorite"];
        item.sessionDetails = [result stringForColumn:@"sessionDetails"];
        [scheduleContent addObject:item];
    }
    [databaseFile close];
}

- (void)setFavoriteStatus:(BOOL)status Where:(NSInteger)identificator{
    databaseFile = [FMDatabase databaseWithPath:databasePath];
    [databaseFile open];
    NSString *command = [NSString stringWithFormat:@"UPDATE sessions SET favorite = %d WHERE sessionId = %d",status,identificator];
    [databaseFile executeUpdate:command];
    [databaseFile close];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger numberOfRows = 0;
    for (ScheduleItem *item in scheduleContent){
        if (item.day == (section + 1)){
            numberOfRows = numberOfRows + 1;
        }
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BOOL canBeFavorite = false;
    ScheduleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScheduleCell"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ScheduleCellView" owner:nil options:nil] objectAtIndex:0];
    }
    
    
    
    NSInteger numberOfRowsInPreviousSections = 0;
    NSInteger sectionCounter = 0;
    while (sectionCounter < indexPath.section) {
        numberOfRowsInPreviousSections = numberOfRowsInPreviousSections + [self tableView:tableView numberOfRowsInSection:sectionCounter];
        sectionCounter++;
    }
    
    
    ScheduleItem *currentItem = [scheduleContent objectAtIndex:(numberOfRowsInPreviousSections+indexPath.row)];
    cell.name.text = currentItem.name;
    cell.time.text = [NSString stringWithFormat:@"Time: %@", currentItem.time];
    cell.room.text = [NSString stringWithFormat:@"Room: %@", currentItem.room];
    
    
    NSString *imageName = [[NSString alloc] init];
    //colorizing cell
    if ([currentItem.name rangeOfString:@"Opening Ceremony"].length) {
        imageName = @"opening.png";
        cell.userInteractionEnabled = false;
    }else if([currentItem.name rangeOfString:@"Registration"].length){
        imageName = @"registration.png";
        cell.userInteractionEnabled = false;
    }else if([currentItem.type rangeOfString:@"Parallel"].length){
        if([currentItem.name rangeOfString:@"Workshop"].length){
            cell.contentView.backgroundColor = [UIColor colorWithRed: 1.0 green: 0.27 blue: 0.0 alpha: 0.2];
            imageName = @"workshop.png";
            cell.userInteractionEnabled = true;
            canBeFavorite = true;
        }else if([currentItem.name rangeOfString:@"Panel"].length){
            cell.contentView.backgroundColor = [UIColor colorWithRed:0.15 green:0.84 blue:0.48 alpha:0.2];
            imageName = @"pannel.png";
            cell.userInteractionEnabled = true;
            canBeFavorite = true;
        }else if([currentItem.name rangeOfString:@"Invited Speaker"].length){
            cell.contentView.backgroundColor = [UIColor colorWithRed: 0.49 green: 0.519 blue: 0.75 alpha: 0.3];
            imageName = @"keynote.png";
            cell.userInteractionEnabled = true;
            canBeFavorite = true;
        }else{
            imageName = @"event.png";
            cell.contentView.backgroundColor = [UIColor colorWithRed:0.85 green:0.24 blue:0.48 alpha:0.2];
            cell.userInteractionEnabled = true;
            canBeFavorite = true;
        }
    }else if([currentItem.name rangeOfString:@"Coffee"].length){
        cell.contentView.backgroundColor = [UIColor colorWithRed: 0.87 green: 0.72 blue: 0.52 alpha: 0.2];
        imageName = @"coffee.png";
        cell.userInteractionEnabled = false;
    }else if([currentItem.name rangeOfString:@"Lunch"].length || [currentItem.name rangeOfString:@"Dinner"].length){
        cell.contentView.backgroundColor = [UIColor whiteColor];
        imageName = @"launch.png";
        cell.userInteractionEnabled = false;
    }else if([currentItem.name rangeOfString:@"Invited Speaker"].length){
        cell.contentView.backgroundColor = [UIColor colorWithRed: 0.49 green: 0.519 blue: 0.75 alpha: 0.3];
        imageName = @"keynote.png";
        cell.userInteractionEnabled = true;
        canBeFavorite= true;
    }
    else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
        imageName = @"event.png";
        cell.userInteractionEnabled = false;
    }
    
    
    cell.image.image = [UIImage imageNamed:imageName];
    if(canBeFavorite == true){
        if(currentItem.isFavorite == true){
            cell.favoriteStatus.image = [UIImage imageNamed:@"favorite_tag_red.png"];
        }else{
            cell.favoriteStatus.image = [UIImage imageNamed:@"unfavorite_tag_red.png"];
        }
    }else{
        cell.favoriteStatus.image = nil;
    }
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *returnValue = [[NSString alloc] init];
    switch (section) {
        case 0:
            returnValue = @"MONDAY, JULY 15";
            break;
        case 1:
            returnValue = @"TUESDAY, JULY 16";
            break;
        case 2:
            returnValue = @"WEDNESDAY, JULY 17";
            break;
        case 3:
            returnValue = @"THURSDAY, JULY 18";
            break;
        default:
            returnValue = @"";
            break;
    }
    return returnValue;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ScheduleDetailViewController *nextViewController = [[ScheduleDetailViewController alloc] initWithNibName:@"ScheduleDetailViewController" bundle:nil];
    nextViewController.currentNavController = self.currentNavController;
    
    NSInteger numberOfRowsInPreviousSections = 0;
    NSInteger sectionCounter = 0;
    while (sectionCounter < indexPath.section) {
        numberOfRowsInPreviousSections = numberOfRowsInPreviousSections + [self tableView:tableView numberOfRowsInSection:sectionCounter];
        sectionCounter++;
    }
    
    nextViewController.itemToDisplay = [scheduleContent objectAtIndex:(numberOfRowsInPreviousSections + indexPath.row)];
    
    // and push it onto the 'navigation stack'
    [self.currentNavController pushViewController:nextViewController animated:YES];
}

@end
