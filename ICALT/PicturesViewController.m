//
//  PicturesViewController.m
//  ICALT
//
//  Created by Vesa Vlad on 4/27/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//
#import "POIViewController.h"
#import "PicturesViewController.h"

@interface PicturesViewController ()
@property (nonatomic, strong) NSArray *contentList;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewControllers;
@end


@implementation PicturesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"POIContent" ofType:@"plist"];
    self.contentList = [NSArray arrayWithContentsOfFile:path];
    NSUInteger numberPages = self.contentList.count;
    
    // view controllers are created lazily
    // in the meantime, load the array with placeholders which will be replaced on demand
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < numberPages; i++)
    {
		[controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    // a page is the width of the scroll view
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize =
    CGSizeMake(CGRectGetWidth(self.scrollView.frame) * numberPages, CGRectGetHeight(self.scrollView.frame));
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    self.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    self.pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    self.pageControl.numberOfPages = numberPages;
    self.pageControl.currentPage = 0;
    
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    //
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    [self loadScrollViewWithPage:2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// at the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = CGRectGetWidth(self.scrollView.frame);
    NSUInteger page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 2];
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    [self loadScrollViewWithPage:page + 2];
    
    // a possible optimization would be to unload the views+controllers which are no longer visible
}

- (void)loadScrollViewWithPage:(NSUInteger)page
{

    if (page >= self.contentList.count)
        return;
    
    // replace the placeholder if necessary
    POIViewController *poiController = [self.viewControllers objectAtIndex:page];
    if ((NSNull *)poiController == [NSNull null])
    {
        poiController = [[POIViewController alloc] initWithNibName:@"PointOfInterestView" bundle:Nil];
        [self.viewControllers replaceObjectAtIndex:page withObject:poiController];
    }
    
    // add the controller's view to the scroll view
    if (poiController.view.superview == nil)
    {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = CGRectGetWidth(frame) * page;
        frame.origin.y = 0;
        poiController.view.frame = frame;
        
        [self addChildViewController:poiController];
        [self.scrollView addSubview:poiController.view];
        [poiController didMoveToParentViewController:self];
        
        NSDictionary *numberItem = [self.contentList objectAtIndex:page];
        poiController.backgroundImage.image = [UIImage imageNamed:[numberItem valueForKey:@"poiImage"]];
        poiController.titleText.text = [numberItem valueForKey:@"poiTitleText"];
        poiController.descriptionText.text = [numberItem valueForKey:@"poiDescriptionText"];
    }

}

@end
