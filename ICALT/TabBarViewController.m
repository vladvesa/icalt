//
//  TabBarViewController.m
//  ICALT
//
//  Created by Vlad Vesa on 4/15/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "TabBarViewController.h"
#import "MainViewController.h"
#import "ScheduleNavigationViewController.h"
#import "MapViewController.h"
#import "FavoritesViewController.h"
#import "PicturesViewController.h"
#import "AuthorsViewController.h"
@interface TabBarViewController ()

@end

@implementation TabBarViewController
- (id)init {
    if (self = [super init]) {
        // Create and configure the Dashboard VC and tab bar item
        MainViewController *mainViewController = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:[NSBundle mainBundle]];
        mainViewController.title = @"News";
        UIImage *selectedIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"twitter-icon.png"]
                                       andGradientOverlay:[UIImage imageNamed:@"selectedtabbaritemgradient"]];
        UIImage *tabBarIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"twitter-icon.png"]
                                     andGradientOverlay:[UIImage imageNamed:@"tabbaritemgradient"]];
        [mainViewController.tabBarItem setFinishedSelectedImage:selectedIcon
                                     withFinishedUnselectedImage:tabBarIcon];
        
        
        
        
        ScheduleNavigationViewController *scheduleNavigationViewController = [[ScheduleNavigationViewController alloc] initWithNibName:@"ScheduleNavigationViewController" bundle:[NSBundle mainBundle]];
        UINavigationController *scheduleNavController = [[UINavigationController alloc] initWithRootViewController:scheduleNavigationViewController];
        scheduleNavigationViewController.currentNavController = scheduleNavController;
        scheduleNavController.navigationBar.tintColor = [UIColor colorWithRed:38/255.0 green:38/255.0 blue:38/255.0 alpha:1.0];
        
        scheduleNavigationViewController.title = @"Schedule";
        selectedIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"calendar-icon.png"]
                               andGradientOverlay:[UIImage imageNamed:@"selectedtabbaritemgradient"]];
        tabBarIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"calendar-icon.png"]
                             andGradientOverlay:[UIImage imageNamed:@"tabbaritemgradient"]];
        [scheduleNavigationViewController.tabBarItem setFinishedSelectedImage:selectedIcon withFinishedUnselectedImage:tabBarIcon];
         
        
        
        MapViewController *mapController = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:[NSBundle mainBundle]];
        UINavigationController *mapNavController = [[UINavigationController alloc] initWithRootViewController:mapController];
        mapNavController.navigationBar.tintColor = [UIColor colorWithRed:38/255.0 green:38/255.0 blue:38/255.0 alpha:1.0];
        
        mapController.currentNavController = mapNavController;
        
        
        mapController.title = @"Map";
        selectedIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"map-icon.png"]
                               andGradientOverlay:[UIImage imageNamed:@"selectedtabbaritemgradient"]];
        tabBarIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"map-icon.png"]
                             andGradientOverlay:[UIImage imageNamed:@"tabbaritemgradient"]];
        [mapController.tabBarItem setFinishedSelectedImage:selectedIcon withFinishedUnselectedImage:tabBarIcon];
        
       
         // Create and configure the Grades VC and tab bar item
         AuthorsViewController *authorsController = [[AuthorsViewController alloc] initWithNibName:@"AuthorsViewController" bundle:[NSBundle mainBundle]];
         authorsController.title = @"Speakers";
         selectedIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"authors-icon.png"]
         andGradientOverlay:[UIImage imageNamed:@"selectedtabbaritemgradient"]];
         tabBarIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"authors-icon.png"]
         andGradientOverlay:[UIImage imageNamed:@"tabbaritemgradient"]];
         [authorsController.tabBarItem setFinishedSelectedImage:selectedIcon
         withFinishedUnselectedImage:tabBarIcon];
         
         // Create and configure the Forums VC and tab bar item
         PicturesViewController *picturesController = [[PicturesViewController alloc] initWithNibName:@"PicturesViewController" bundle:[NSBundle mainBundle]];
         picturesController.title = @"POI";
         selectedIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"pictures-icon.png"]
         andGradientOverlay:[UIImage imageNamed:@"selectedtabbaritemgradient"]];
         tabBarIcon = [self tabBarIconWithImage:[UIImage imageNamed:@"pictures-icon.png"]
         andGradientOverlay:[UIImage imageNamed:@"tabbaritemgradient"]];
         [picturesController.tabBarItem setFinishedSelectedImage:selectedIcon
         withFinishedUnselectedImage:tabBarIcon];
        
        /* Since the iPad tab bar background doesn't have the vertical lines (because the tab bar items don't line
         up cleanly – it's complicated), set the background and selection indicator image for the device we're
         running on */
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
             [[self tabBar] setBackgroundImage:[UIImage imageNamed:@"ipadtabbarbackground"]];
             [[self tabBar] setSelectionIndicatorImage:[UIImage imageNamed:@"ipadtabbarselection"]];
        } else {
             [[self tabBar] setBackgroundImage:[UIImage imageNamed:@"tabbarbackground"]];
             [[self tabBar] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbarselection"]];
        }
        
        // Set the regular and selected text attributes for all of the tab bar items
        [[UITabBarItem appearance] setTitleTextAttributes:@{ UITextAttributeTextColor: [UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0],
                          UITextAttributeTextShadowColor : [UIColor blackColor],
                         UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetMake(0.0f, -1.0f)] }
                                                 forState:UIControlStateNormal];
        
        [[UITabBarItem appearance] setTitleTextAttributes:@{ UITextAttributeTextColor        : [UIColor whiteColor],
                         UITextAttributeTextShadowColor  : [UIColor blackColor],
                         UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)] }
                                                 forState:UIControlStateSelected];
        
        [self setViewControllers:@[mainViewController,scheduleNavController, authorsController, mapNavController,picturesController]];
        //[self setViewControllers:@[ dashboardController, assignmentsController, participantsController, gradesController, forumController ]];
    }
    
    return self;
}

- (UIImage *)tabBarIconWithImage:(UIImage *)img andGradientOverlay:(UIImage *)gradient {
    // Create a 34x34 graphics context
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(34, 34), NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    // Draw the gradient we're passed over the center 30x30 points
    CGContextDrawImage(context, CGRectMake(2, 2, 30, 30), gradient.CGImage);
    // Composite our image atop it such that the gradient is masked to the image
    [img drawAtPoint:CGPointMake(2, 2) blendMode:kCGBlendModeDestinationAtop alpha:1.0];
    
    // Grab the current image from the graphics context
    UIImage *pic = UIGraphicsGetImageFromCurrentImageContext();
    
    // Clear out the context
    CGContextClearRect(context, CGRectMake(0, 0, 34, 34));
    // Set a 2 pixel black shadow
    CGContextSetShadowWithColor(context, CGSizeMake(0, 0), 2, [UIColor blackColor].CGColor);
    // Draw the previous contents of the context (the gradient masked to the image) with the shadow we just set
    CGContextDrawImage(context, CGRectMake(0, 0, 34, 34), pic.CGImage);
    // Get the final image and destroy the context
    pic = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Scale and rotate the final image so it appears right-side-up and retina (or not) as needed
    pic = [UIImage imageWithCGImage:[pic CGImage] scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationDownMirrored];
    
    return pic;
    
}

// Allow rotation on the iPad and enforce portrait orientation on other devices
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
    } else {
        return YES;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
