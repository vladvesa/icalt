	//
//  ViewController.m
//  ICALT
//
//  Created by Vlad Vesa on 4/10/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "MainViewController.h"
#import "AFJSONRequestOperation.h"
#import "AFOAuth2Client.h"
#import "UIImageView+AFNetworking.h"
@interface MainViewController ()

@end
@implementation MainViewController


- (IBAction)refreshButtonClicked {
    [self contentFetchTweets];
    
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];

    //Set up the moving pictures in the background
    double ratio = [[UIScreen mainScreen] bounds].size.height / 1024.0;
    floatingBackground = [[CALayer alloc] init];
    floatingBackground.anchorPoint = CGPointMake(0, 0);
    floatingBackground.frame = CGRectMake(0, 0, 1536 * ratio, 1024 * ratio);
    if ([[UIScreen mainScreen] scale] == 2.0) {
        floatingBackground.contents = (id)[[UIImage imageNamed:@"china-wall-bg.png"] CGImage];
    } else {
        floatingBackground.contents = (id)[[UIImage imageNamed:@"china-wall-bg.png"] CGImage];
    }
    floatingBackground.backgroundColor = [[UIColor redColor] CGColor];
    floatingBackground.zPosition = [self.view layer].zPosition - 1;
    floatingBackground.rasterizationScale = [[UIScreen mainScreen] scale];
    floatingBackground.shouldRasterize = YES;
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.fromValue = [floatingBackground valueForKey:@"position"];
    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(-1 * ((1280 * ratio) - [[UIScreen mainScreen] bounds].size.width), 0)];
    animation.duration = 35.0;
    animation.delegate = self;
    
    floatingBackground.position = CGPointMake(-1 * ((1280 * ratio) - [[UIScreen mainScreen] bounds].size.width), 0);
    
    [floatingBackground addAnimation:animation forKey:@"position"];
    [[self.view layer] addSublayer:floatingBackground];
    self.view.layer.masksToBounds = YES;
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    twitterMiniView.frame = CGRectMake(0, 130, self.view.bounds.size.width, self.view.bounds.size.height-130);
    twitterMiniView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:twitterMiniView];
    [self fetchTweets];
    if([tweets count] == 0){
        [self contentFetchTweets];
    }
}


-(void)contentFetchTweets{
#ifdef USE_DEFAULT_TWITTFETCH_SOURCE
    /*
     * this version of fetching tweets is not working for China
     */
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"];
    AFOAuth2Client *oauthClient = [AFOAuth2Client clientWithBaseURL:url clientID:@"gtBfTrRGm6hgxQqka72xrQ" secret:@"uRKabyY7hMAdbaU3awxe7cUrVgapCsaY16b5zHII"];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setValue:@"client_credentials" forKey:@"grant_type"];
    [oauthClient authenticateUsingOAuthWithPath:@"/oauth2/token" parameters:dictionary  success:^(AFOAuthCredential *credential) {
        //NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/user_timeline.json?count=100&screen_name=icalt2013&contributor_details=true"];
        NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/search/tweets.json?q=icalt+2013&since_id=0&count=20"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"GET"];
        NSString *bearer = [[NSString alloc] initWithFormat:@"Bearer %@",credential.accessToken];
        [request addValue:bearer forHTTPHeaderField:@"Authorization"];
       
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            NSLog(@"%@",JSON);
            
            //if there are tweets stored inside core data delete them and fetch new ones because they could be obsolete
            if ([[self managedObjectContext] tryLock] == TRUE)
            {
                [[self managedObjectContext] lock];
                if(tweets != nil)
                {
                    for (Tweet *t in tweets) {
                        [[self managedObjectContext] deleteObject:t];
                    }
                    [tweets removeAllObjects];
                }
            
                for (NSDictionary *extractedTweet in [JSON objectForKey:@"statuses"]) {
                    Tweet *tweet = (Tweet *)[NSEntityDescription insertNewObjectForEntityForName:@"Tweet" inManagedObjectContext:[self managedObjectContext]];
                    if(([[extractedTweet objectForKey:@"retweet_count"] integerValue] >= 1) && ([extractedTweet objectForKey:@"retweeted_status"] != nil)){
                        NSDictionary *extractedReTweet = [[NSDictionary alloc] initWithDictionary:[extractedTweet objectForKey:@"retweeted_status"]];
                        tweet.tweetText = [extractedReTweet objectForKey:@"text"];
                        NSDictionary *userInfo = [(NSDictionary *)extractedReTweet objectForKey:@"user"];
                        tweet.username = [userInfo objectForKey:@"name"];
                        tweet.userPictureUrl = [userInfo objectForKey:@"profile_image_url"];
                    }else{
                        tweet.tweetText = [extractedTweet objectForKey:@"text"];
                        NSDictionary *userInfo = [(NSDictionary *)extractedTweet objectForKey:@"user"];
                        tweet.username = [userInfo objectForKey:@"name"];
                        tweet.userPictureUrl = [userInfo objectForKey:@"profile_image_url"];
                    }
                    
                    NSError *error = nil;
                    if(![[self managedObjectContext] save:&error])
                    {
#ifdef DEBUG
                        NSLog(@"%s: An error occured: %@",__func__, [error description]);
#endif
                    }
                    
                }
                [[self managedObjectContext] unlock];
                [self fetchTweets];
                
                [twitterMiniView reloadData];
#ifdef DEBUG
            NSLog(@"%s: Result fetched from twitter:%@", __func__, JSON);
#endif
            }
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON){
            UIAlertView *allert = [[UIAlertView alloc] initWithTitle:@"I'm sorry" message:@"No internet connection available, we will load data stored in cache if there is any" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [allert show];
#ifdef DEBUG
            NSLog(@"%s: An error occured: %@",__func__, [error description]);
#endif
            if (tweets == Nil) {
                [self fetchTweets];
                [twitterMiniView reloadData];
            }
        }];
        [operation start];
        
    } failure:^(NSError *error) {
        UIAlertView *allert = [[UIAlertView alloc] initWithTitle:@"I'm sorry" message:@"No internet connection available, we will load data stored in cache if there is any" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [allert show];
        #ifdef DEBUG
        NSLog(@"%s: An error occured: %@",__func__, [error description]);
        #endif
        //if no connection is available or if an error occured we will try to fetch tweets from core data
        if (tweets == Nil){
            [self fetchTweets];
            [twitterMiniView reloadData];            
        }
    }];    
#else
    NSURL *url = [NSURL URLWithString:@"http://dev.cm.upt.ro/vladvesa/icalt/"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        #ifdef DEBUG
        NSLog(@"%@", JSON);
        #endif
        //if there are tweets stored inside core data delete them and fetch new ones because they could be obsolete
        if ([[self managedObjectContext] tryLock] == TRUE)
        {
            [[self managedObjectContext] lock];
            if(tweets != nil)
            {
                for (Tweet *t in tweets) {
                    [[self managedObjectContext] deleteObject:t];
                }
                [tweets removeAllObjects];
            }
            
            for (NSDictionary *extractedTweet in [JSON objectForKey:@"statuses"]) {
                Tweet *tweet = (Tweet *)[NSEntityDescription insertNewObjectForEntityForName:@"Tweet" inManagedObjectContext:[self managedObjectContext]];
                if(([[extractedTweet objectForKey:@"retweet_count"] integerValue] >= 1) && ([extractedTweet objectForKey:@"retweeted_status"] != nil)){
                    NSDictionary *extractedReTweet = [[NSDictionary alloc] initWithDictionary:[extractedTweet objectForKey:@"retweeted_status"]];
                    tweet.tweetText = [extractedReTweet objectForKey:@"text"];
                    NSDictionary *userInfo = [(NSDictionary *)extractedReTweet objectForKey:@"user"];
                    tweet.username = [userInfo objectForKey:@"name"];
                    tweet.userPictureUrl = [userInfo objectForKey:@"profile_image_url"];
                }else{
                    tweet.tweetText = [extractedTweet objectForKey:@"text"];
                    NSDictionary *userInfo = [(NSDictionary *)extractedTweet objectForKey:@"user"];
                    tweet.username = [userInfo objectForKey:@"name"];
                    tweet.userPictureUrl = [userInfo objectForKey:@"profile_image_url"];
                }
                
                NSError *error = nil;
                if(![[self managedObjectContext] save:&error])
                {
                    #ifdef DEBUG
                    NSLog(@"%s: An error occured: %@",__func__, [error description]);
                    #endif
                }
                
            }
            [[self managedObjectContext] unlock];
            [self fetchTweets];
            
            [twitterMiniView reloadData];
            #ifdef DEBUG
            NSLog(@"%s: Result fetched from twitter:%@", __func__, JSON);
            #endif
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON){
        UIAlertView *allert = [[UIAlertView alloc] initWithTitle:@"I'm sorry" message:@"No internet connection available, we will load data stored in cache if there is any" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [allert show];
        #ifdef DEBUG
        NSLog(@"%s: An error occured: %@",__func__, [error description]);
        #endif
        if (tweets == Nil) {
            [self fetchTweets];
            [twitterMiniView reloadData];
        }
    }];
    [operation start];
    
#endif
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Core animation handling

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag;
{
    [floatingBackground removeAllAnimations];
    
    if (self.isViewLoaded && self.view.window) {
        image++;
        if (image == 4) //Change this value to add support for more images
            image = 1;
        
        double ratio = [[UIScreen mainScreen] bounds].size.height / 1024.0;
        
        //Determine whether we're going left to right or right to left
        CGPoint newPoint;
        if (CGPointEqualToPoint(floatingBackground.position, CGPointMake(-1 * ((1280 * ratio) - [[UIScreen mainScreen] bounds].size.width), 0)))
            newPoint = CGPointMake(0, 0);
        else
            newPoint = CGPointMake(-1 * ((1280 * ratio) - [[UIScreen mainScreen] bounds].size.width), 0);
        
        if ([[UIScreen mainScreen] scale] == 2.0) {
            //floatingBackground.contents = (id)[[UIImage imageNamed:[NSString stringWithFormat:@"%iretina.jpg", image]] CGImage];
        } else {
            //floatingBackground.contents = (id)[[UIImage imageNamed:[NSString stringWithFormat:@"%i.jpg", image]] CGImage];
        }
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
        animation.fromValue = [floatingBackground valueForKey:@"position"];
        animation.toValue = [NSValue valueWithCGPoint:newPoint];
        animation.duration = 35.0;
        animation.delegate = self;
        
        floatingBackground.position = newPoint;
        
        [floatingBackground addAnimation:animation forKey:@"position"];
    }
}

- (void) fetchTweets {
        [[self managedObjectContext] lock];
    if ([self managedObjectContext] == Nil) {
        NSLog(@"NIL");
    }else{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Tweet" inManagedObjectContext:[self managedObjectContext]];
        [fetchRequest setEntity:entity];
        
        NSError *error = nil;
        tweets = [[[self managedObjectContext] executeFetchRequest:fetchRequest error:&error] mutableCopy];
        if ([tweets count] == 0) {
#ifdef DEBUG
            NSLog(@"%s: An error occured: %@",__func__, [error description]);
#endif
        }
    }
        [[self managedObjectContext] unlock];
}

#pragma mark - tableViewDataSource required methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [tweets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"TweetCell";
    Tweet *tweet = [tweets objectAtIndex:indexPath.row];
    TweeterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TwitterCellView" owner:nil options:nil] objectAtIndex:0];
    }
    cell.tweetUserInfo.text = tweet.username;
    cell.tweetText.text = tweet.tweetText;
    cell.tweetUserImage.layer.cornerRadius = 4;
    cell.tweetUserImage.clipsToBounds = YES;
    cell.tweetUserImage.layer.borderWidth = 0.15;
    
    [cell.tweetUserImage setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:tweet.userPictureUrl]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *userImage) {
        cell.tweetUserImage.image = userImage;        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        cell.tweetUserImage.image = [UIImage imageNamed:@"unknown_user_image.gif"];
    }];
     
    tableView.rowHeight = cell.frame.size.height;
    return cell;
}


@end
