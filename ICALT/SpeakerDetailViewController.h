//
//  SpeakerDetailViewController.h
//  ICALT
//
//  Created by Vesa Vlad on 5/20/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakerDetailViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *speakerImage;
@property (strong, nonatomic) IBOutlet UITextField *speakerName;
@property (strong, nonatomic) IBOutlet UITextField *speakerFunction;
@property (strong, nonatomic) IBOutlet UITextField *speakerArticke;
@property (strong, nonatomic) IBOutlet UITextView *speakerBIO;

@end
