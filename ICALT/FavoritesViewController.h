//
//  FavoritesViewController.h
//  ICALT
//
//  Created by Vesa Vlad on 4/26/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesViewController : UIViewController{
    @private
    IBOutlet UINavigationBar *navigationBar;
    
}

@end
