//
//  AuthorsViewController.h
//  ICALT
//
//  Created by Vesa Vlad on 5/16/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface AuthorsViewController : UIViewController <UIScrollViewDelegate>

@end
