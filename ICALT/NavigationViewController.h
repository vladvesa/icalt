//
//  NavigationViewController.h
//  ICALT
//
//  Created by Vesa Vlad on 6/25/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationViewController : UINavigationController

@end
