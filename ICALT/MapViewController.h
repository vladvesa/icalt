//
//  MapViewController.h
//  ICALT
//
//  Created by Vlad Vesa on 4/15/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface MapViewController : UIViewController <MKAnnotation,MKMapViewDelegate>{
    @private 
}

@property (strong, nonatomic) IBOutlet MKMapView *conferenceMap;
@property (strong, nonatomic) UINavigationController *currentNavController;
@end
