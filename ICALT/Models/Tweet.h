//
//  Tweet.h
//  ICALT
//
//  Created by Vesa Vlad on 5/6/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Tweet : NSManagedObject

@property (nonatomic, retain) NSString * tweetText;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * userPictureUrl;

@end
