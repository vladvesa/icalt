//
//  Tweet.m
//  ICALT
//
//  Created by Vesa Vlad on 5/6/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "Tweet.h"


@implementation Tweet

@dynamic tweetText;
@dynamic username;
@dynamic userPictureUrl;

@end
