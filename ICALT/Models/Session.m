//
//  Session.m
//  ICALT
//
//  Created by Vesa Vlad on 5/12/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "Session.h"


@implementation Session

@dynamic idSessionItem;
@dynamic sessionItemName;
@dynamic sessionItemDescription;
@dynamic sessionItemDay;
@dynamic sessionItemStartHour;
@dynamic sessionItemEndHour;
@dynamic sessionItemType;
@dynamic sessionItemRoomPlace;
@dynamic sessionItemChair;
@dynamic sessionIsFavorite;

@end
