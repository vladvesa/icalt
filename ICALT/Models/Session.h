//
//  Session.h
//  ICALT
//
//  Created by Vesa Vlad on 5/12/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Session : NSManagedObject

@property (nonatomic, retain) NSNumber * idSessionItem;
@property (nonatomic, retain) NSString * sessionItemName;
@property (nonatomic, retain) NSString * sessionItemDescription;
@property (nonatomic, retain) NSString * sessionItemDay;
@property (nonatomic, retain) NSString * sessionItemStartHour;
@property (nonatomic, retain) NSString * sessionItemEndHour;
@property (nonatomic, retain) NSString * sessionItemType;
@property (nonatomic, retain) NSString * sessionItemRoomPlace;
@property (nonatomic, retain) NSString * sessionItemChair;
@property (nonatomic, retain) NSNumber * sessionIsFavorite;

@end
