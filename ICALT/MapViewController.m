//
//  MapViewController.m
//  ICALT
//
//  Created by Vlad Vesa on 4/15/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "MapViewController.h"
#import "ConferenceVenueMapViewController.h"
@interface MapViewController ()

@end

@implementation MapViewController
@synthesize conferenceMap = _conferenceMap;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.conferenceMap.mapType=MKMapTypeStandard;
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = 39.95834;
    newRegion.center.longitude = 116.366866;
    newRegion.span.latitudeDelta = 0.008997;
    newRegion.span.longitudeDelta = 0.021114;
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 39.95834;
    coordinate.longitude = 116.366866;
     
    //create Pin
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    [annotation setTitle:@"Conference Venue"];
    [annotation setSubtitle:@"19 Xinjiekouwai Street, Haidan District, Beijing, China"];
    [self.conferenceMap addAnnotation:annotation];
    [self.conferenceMap setRegion:newRegion animated:YES];
    [self.conferenceMap selectAnnotation:annotation animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKAnnotationView *) mapView:(MKMapView *) mapView viewForAnnotation:(id ) annotation {
    MKAnnotationView *customAnnotationView=[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
    UIImage *pinImage = [UIImage imageNamed:@"pin.png"];
    [customAnnotationView setImage:pinImage];
    customAnnotationView.canShowCallout = YES;
    UIImageView *leftIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icalt-map-icon.png"]];
    customAnnotationView.leftCalloutAccessoryView = leftIconView;
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [rightButton addTarget:self action:@selector(annotationViewClick:) forControlEvents:UIControlEventTouchUpInside];
    customAnnotationView.rightCalloutAccessoryView = rightButton;
    return customAnnotationView;
}
- (IBAction) annotationViewClick:(id) sender {
    ConferenceVenueMapViewController *nextViewController = [[ConferenceVenueMapViewController alloc] initWithNibName:@"ConferenceVenueMapViewController" bundle:nil];
    // and push it onto the 'navigation stack'
    [self.currentNavController pushViewController:nextViewController animated:YES];
    // and release
}

@end
