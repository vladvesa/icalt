//
//  ScheduleDetailViewController.m
//  ICALT
//
//  Created by Vesa Vlad on 6/25/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "ScheduleDetailViewController.h"
#import "ScheduleNavigationViewController.h"
@interface ScheduleDetailViewController ()

@end

@implementation ScheduleDetailViewController
@synthesize itemToDisplay = _itemToDisplay;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Session Details";
    self.name.text = self.itemToDisplay.name;
    self.time.text = [NSString stringWithFormat:@"Time: %@", self.itemToDisplay.time];
    self.room.text = [NSString stringWithFormat:@"Room: %@", self.itemToDisplay.room];
    
    //set chair name informations
    if (self.itemToDisplay.chairName != NULL) {
        self.chair.text = [NSString stringWithFormat:@"Chair: %@", self.itemToDisplay.chairName];
    }else{
        self.chair.hidden = true;
    }
    
    //set keynote informations
    if (self.itemToDisplay.keynote != NULL) {
        self.keynote.text = [NSString stringWithFormat:@"Keynote: %@", self.itemToDisplay.keynote];
    }else{
        self.keynote.hidden = true;
    }
    
    //set favorite image when flag is set or reset it when flag is not set
    if (self.itemToDisplay.isFavorite == true) {
        self.favoriteImage.image = [UIImage imageNamed:@"favorite_tag_red.png"];
        [self.favoriteButton setTitle:@"Remove favorite" forState:UIControlStateNormal];
        [self.favoriteButton setTitle:@"Remove favorite" forState:UIControlStateSelected];
#ifdef DEBUG
        NSLog(@"is favorite");
#endif
    }else{
        self.favoriteImage.image = [UIImage imageNamed:@"unfavorite_tag_red.png"];
        [self.favoriteButton setTitle:@"Add favorite" forState:UIControlStateNormal];
        [self.favoriteButton setTitle:@"Add favorite" forState:UIControlStateSelected];
#ifdef DEBUG
        NSLog(@"is not favorite");
#endif
    }
    self.sessionDetails.text = self.itemToDisplay.sessionDetails;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ShowMap:(UIButton *)sender {
    ConferenceVenueMapViewController *nextViewController = [[ConferenceVenueMapViewController alloc] initWithNibName:@"ConferenceVenueMapViewController" bundle:nil];
    // and push it onto the 'navigation stack'
    [self.currentNavController pushViewController:nextViewController animated:YES];
    // and release
}

- (IBAction)setFavoriteStatus:(UIButton *)sender {

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"New status" message:[NSString stringWithFormat:@"New status set for: %@",self.itemToDisplay.name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    self.itemToDisplay.isFavorite = !self.itemToDisplay.isFavorite;
    ScheduleNavigationViewController *scheduleNav = [[ScheduleNavigationViewController alloc] init];
    [scheduleNav setFavoriteStatus:self.itemToDisplay.isFavorite Where:self.itemToDisplay.sessionId];
    [self viewDidLoad];
}
@end
