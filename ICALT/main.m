//
//  main.m
//  ICALT
//
//  Created by Vlad Vesa on 4/10/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
