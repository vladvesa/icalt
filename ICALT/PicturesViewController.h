//
//  PicturesViewController.h
//  ICALT
//
//  Created by Vesa Vlad on 4/27/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicturesViewController : UIViewController <UIScrollViewDelegate>

@end
