//
//  ScheduleNavigationViewController.h
//  ICALT
//
//  Created by Vlad Vesa on 4/15/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
@interface ScheduleNavigationViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    @private
    NSString *databasePath;
    FMDatabase *databaseFile;
    NSMutableArray *scheduleContent;
    IBOutlet UITableView *tableViewDisplay;
}
@property (strong, nonatomic) UINavigationController *currentNavController;
- (void)setFavoriteStatus:(BOOL)status Where: (NSInteger)identificator;
@end
