//
//  ViewController.h
//  ICALT
//
//  Created by Vlad Vesa on 4/10/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <Twitter/Twitter.h>
#import "Tweet.h"
#import "TweeterCell.h"
@interface MainViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
@private
    CALayer *floatingBackground;
    NSMutableArray *tweets;
    IBOutlet UITableView *twitterMiniView;
    int image;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
- (IBAction)refreshButtonClicked;
@end

