//
//  TabBarViewController.h
//  ICALT
//
//  Created by Vlad Vesa on 4/15/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarViewController : UITabBarController

@end
