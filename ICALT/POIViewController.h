//
//  POIViewController.h
//  ICALT
//
//  Created by Vesa Vlad on 5/16/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface POIViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UITextField *titleText;
@property (strong, nonatomic) IBOutlet UITextView *descriptionText;
@end
