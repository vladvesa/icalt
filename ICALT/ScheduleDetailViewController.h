//
//  ScheduleDetailViewController.h
//  ICALT
//
//  Created by Vesa Vlad on 6/25/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConferenceVenueMapViewController.h"
#import "ScheduleItem.h"
@interface ScheduleDetailViewController : UIViewController
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) UINavigationController *currentNavController;
@property (nonatomic, strong) ScheduleItem *itemToDisplay;
- (IBAction)ShowMap:(UIButton *)sender;
- (IBAction)setFavoriteStatus:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteImage;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UILabel *room;
@property (strong, nonatomic) IBOutlet UILabel *chair;
@property (strong, nonatomic) IBOutlet UILabel *keynote;
@property (strong, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UITextView *sessionDetails;
@end
