//
//  ConferenceVenueMapViewController.m
//  ICALT
//
//  Created by Vesa Vlad on 6/25/13.
//  Copyright (c) 2013 UPT. All rights reserved.
//

#import "ConferenceVenueMapViewController.h"

@interface ConferenceVenueMapViewController ()

@end

@implementation ConferenceVenueMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Venue map";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
