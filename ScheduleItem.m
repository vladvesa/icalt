//
//  ScheduleItem.m
//  ICALT 2012
//
//  Created by Vlad Vesa on 6/6/12.
//  Copyright (c) 2012 UPT. All rights reserved.
//

#import "ScheduleItem.h"

@implementation ScheduleItem
@synthesize sessionId = _sessionId;
@synthesize name = _name;
@synthesize type = _type;
@synthesize room = _room;
@synthesize day = _day;
@synthesize time = _time;
@synthesize chairName = _chairName;
@synthesize keynote = _keynote;
@synthesize isFavorite = _isFavorite;
@synthesize map = _map;
@synthesize sessionDetails = _sessionDetails;

@end
